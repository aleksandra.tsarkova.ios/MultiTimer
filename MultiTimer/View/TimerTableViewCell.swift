//
//  TimerTableViewCell.swift
//  MultiTimer
//
//  Created by Александра on 07.07.2021.
//

import UIKit

//MARK: - Cell Protocols

protocol TimerOff {
    func deleteTimer(indexPath: IndexPath)
}

// MARK: - TimerTableViewCell

final class TimerTableViewCell: UITableViewCell {
    
    // MARK: - Private property
    
    private var timerInfo: TimerInfo? {
        didSet {
            nameLabel.text = timerInfo?.name
            updateTime()
        }
    }
    
    var delegate: TimerOff?
    
    private var currentTimer: Timer?
    
    var startTime: Date? = Date()
    private var endTime: Date?
    
    private let nameLabel = UILabel()
    private let timeLabel = UILabel()
    
    // MARK: - Life cycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        timeLabel.textColor = .gray
        contentView.addSubview(nameLabel)
        contentView.addSubview(timeLabel)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        timeLabel.textColor = .gray
        contentView.addSubview(nameLabel)
        contentView.addSubview(timeLabel)
    }
    
    override func layoutSubviews() {
        nameLabel.frame = CGRect(
            x: 10,
            y: 5,
            width: 200,
            height: contentView.frame.size.height)
        
        timeLabel.textAlignment = .right
        
        timeLabel.frame = CGRect(x: contentView.frame.size.width - 60,
                                 y: 5,
                                 width: 50,
                                 height: contentView.frame.size.height)
    }
    
    // MARK: - Public methods
    
    func configureCell(for info: TimerInfo) {
        timerInfo = info
    }
    
    func updateTime() {
        guard let timer = timerInfo else { return }
        
        let time = timer.timeInSeconds
        endTime = startTime!.addingTimeInterval(TimeInterval(time))
        countTimeForSeconds()
    }
    
    
    
    // MARK: - Private methods
    
    private func timeLeft() -> Double {
        guard let endTime = endTime?.timeIntervalSinceNow else { return 0.0 }
        return endTime
    }
    
    private func countTimeForSeconds() {
        let newTime = timeLeft()
        
        let hours = Int(newTime) / 3600
        let minutes = Int(newTime) / 60 % 60
        let seconds = Int(newTime) % 60
        
        var times: [String] = []
        
        if hours > 0 {
            times.append("\(hours)")
        }
        
        if minutes > 0 {
            times.append("\(minutes)")
        }
        
        if seconds > 0 {
            times.append("\(seconds)")
        } else if hours <= 0 && minutes <= 0 && seconds <= 0 {
            timerInvalidate()
            deleteTableViewRowDelegate()
        }
        timeLabel.text = times.joined(separator: ":")
    }
    
    private func timerInvalidate() {
        currentTimer?.invalidate()
        TimerStore.shared.removeTimerInfo(timerInfo!)
        timerInfo = nil
        currentTimer = nil
        startTime = nil
    }
    
    private func deleteTableViewRowDelegate() {
        if let tableView = self.superview as? UITableView {
            if let indexPath = tableView.indexPath(for: self) {
                delegate?.deleteTimer(indexPath: indexPath)
            } else {
                print("Indexpath could not be acquired from tableview.")
            }
        } else {
            print("Superview couldn't be cast as tableview")
        }
    }
}

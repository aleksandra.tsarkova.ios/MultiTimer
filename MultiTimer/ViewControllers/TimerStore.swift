//
//  TimerStore.swift
//  MultiTimer
//
//  Created by Александра on 09.07.2021.
//

import Foundation

struct TimerStore {
    static var shared = TimerStore()
    
    // MARK: - Private property
    
    private(set) var timers: [TimerInfo] = []
    
    // MARK: - Public methods
    
    mutating func addTimerInfo(_ info: TimerInfo) {
        timers.append(info)
    }
    
    mutating func removeTimerInfo(_ info: TimerInfo) {
        if timers.contains(info) {
            timers.remove(at: timers.firstIndex(of: info)!)
        }
    }
}
